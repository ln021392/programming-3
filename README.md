# Programming 3

Assignment Coursework #3

Module Code:  CS1PC20
Assignment report Title: Coursework 3
Student Number (e.g. 25098635): 30021392
Date (when the work completed): 11/02/2022
Actual hrs spent for the assignment: 10h
Assignment evaluation (3 key points): 
1. I feel as if the lessons should focus more on the specification.
2. I learned what polymorphism is.
3. I also learned how to create OOP code.

Content of the required work

Question 1
What is the significance of using OOP? Demonstrate your in depth understanding of polymorphism with an example.
OOP language permits to break the program into the cycle estimated issues that can be settled effectively (each article in turn). The new innovation guarantees more noteworthy developer efficiency, better nature of programming and lesser upkeep cost. OOP frameworks can be handily redesigned from little to huge frameworks. A genuine illustration of polymorphism, an individual simultaneously can have various attributes. Like a man simultaneously is a dad, a spouse, a worker. So similar individual forces different conduct in various circumstances. This is called polymorphism.


Question 2
Develop your code integrating a polymorphism concept. Follow the tutorial exercise in the class. However, you can extend your codes integrating an inheritance concept. Use comments in your code as much as you can to explain the code you have written.
