void dynamicCastObject() {
	// dynamic cast, this requires that the class is polymorphic, i.e., at least one
	virtual method
		Child c;
	cout << "Child c: " << c.hello() << endl;
	// dynamic cast fails as p1 is not Child; this throws std::bad_cast exception
	Parent p1;
	Child& cr = dynamic_cast<Child&>(p1);
}
int main() {
	try {
		dynamicCastObject();
	}
	catch (exception& b) { // catch std::exception which bad_cast is a child
		cout << "Error :" << b.what() << endl;
	}
	return 0;
}
