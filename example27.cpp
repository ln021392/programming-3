void testFunc(SmartPointer<int> p) {
	// when leaving the function destroy p => count = 2
}
int main()
{
	SmartPointer<int> p(new int()); // count = 1
	*p = 10;
	cout << *p << endl; // This calls the overloaded operator
	auto p2 = p; // calls copy constructor for smart pointer: count = 2
	testFunc(p2); // calls copy constructor for smart pointer: count = 3
	// p and p2 goes out of scope, decrementing count
	return 0;
}
