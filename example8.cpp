#include <string>
#include <iostream>
using namespace std;
typedef enum { // enums from C
	COLOR_BLACK,
	COLOR_BLUE,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BROWN
} hairColor_t;
int main() {
	person boss;
	boss.setName("my Boss");
	boss.setHairColor(COLOR_BLACK);
	boss.print();
	return 0;
}
