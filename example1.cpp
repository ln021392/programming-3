#include <iostream>
// compile using g++ hello-world.cpp -o hello
int main() {
	int x, y, sum1;
	std::cout << "Enter first integer\n"; // Output
	std::cin >> x; // Input
	std::cout << "Enter second integer\n";
	std::cin >> y;
	sum1 = x + y;
	std::cout << "Sum is " << sum1 << std::endl;
	return 0;
}
