#include <string>
using namespace std;
class Car {
public: // visibility, details will be taught later
	int registration;
	string colour;
	int getRegistration() { return registration; }
	string getColour() { return colour; }
	void setRegistration(int pReg) {
		registration = pReg;
	}
	void setColour(string pColour) {
		colour = pColour;
	}
};

