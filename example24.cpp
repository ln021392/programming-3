class PeopleFactory
{
public:
	Person* createPerson
	(vector
		<string> const& p) {
		// check what properties are set
		bool p_car = find(begin(p), end(p), "car") != p.end();
		bool p_works = find(begin(p), end(p), "works") != p.end();
		bool p_studies = find(begin(p), end(p), "studies") != p.end();
		if
			(p_car) {
			if
				(p_works || p_studies) {
				throw exception();
			}
			return new Transformer();
		}
		if
			(p_studies) {
			if
				(p_works) {
				return new PartTimeStudent();
			}
			return new Student();
		}
		if
			(p_works) {
			return new Employee();
		}
		// unknown property choice for now
		throw exception();
	}
};
